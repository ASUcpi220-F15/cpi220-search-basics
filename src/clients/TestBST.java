package clients;

import util.StdOut;
import symbolTable.BST;
import symbolTable.RedBlackBST;

/******************************************************************************
 *  Compilation:  javac TestBST.java
 *  Execution:    java TestBST
 *  Dependencies: BST.java
 *
 *  A test client for BST.java.
 * 
 *  % java -ea TestBST
 *
 ******************************************************************************/

public class TestBST {
    public static void main(String[] args) { 
        String test = "S E A R C H E X A M P L E"; 
        String[] keys = test.split(" "); 
        int N = keys.length; 
        BST<String, Integer> st = new RedBlackBST<String, Integer>();
        for (int i = 0; i < N; i++) 
            st.put(keys[i], i); 

        StdOut.println("size = " + st.size());
        StdOut.println("min  = " + st.min());
        StdOut.println("max  = " + st.max());
        StdOut.println();


        // print keys in order using allKeys()
        StdOut.println("Testing keys()");
        StdOut.println("--------------------------------");
        for (String s : st.keys()) 
            StdOut.println(s + " " + st.get(s)); 
        StdOut.println();

        // print keys in order using select
        StdOut.println("Testing select");
        StdOut.println("--------------------------------");
        for (int i = 0; i < st.size(); i++)
            StdOut.println(i + " " + st.select(i)); 
        StdOut.println();

        // test rank, floor, ceiling
        StdOut.println("key rank floor ceil");
        StdOut.println("-------------------");
        for (char i = 'A'; i <= 'Z'; i++) {
            String s = i + "";
            StdOut.printf("%2s %4d %4s %4s\n", s, st.rank(s), st.floor(s), st.ceiling(s));
        }
        StdOut.println();

        // test range search and range count
        String[] from = { "A", "Z", "X", "0", "B", "C" };
        String[] to   = { "Z", "A", "X", "Z", "G", "L" };
        StdOut.println("range search");
        StdOut.println("-------------------");
        for (int i = 0; i < from.length; i++) {
            StdOut.printf("%s-%s (%2d) : ", from[i], to[i], st.size(from[i], to[i]));
            for (String s : st.keys(from[i], to[i]))
                StdOut.print(s + " ");
            StdOut.println();
        }
        StdOut.println();
        
        //if it is a red-black tree, test to see the tree is balanced
        if (st instanceof RedBlackBST)
        {
        	System.out.println("testing balance");
        	StdOut.println("-------------------");
        	System.out.println(((RedBlackBST<String, Integer>)st).isBalanced() + " " + ((RedBlackBST<String, Integer>)st).is23());
        }        

    }
}
